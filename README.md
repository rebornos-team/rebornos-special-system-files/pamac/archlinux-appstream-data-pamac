# archlinux-appstream-data-pamac

Arch Linux application database for AppStream-based software centers (Fixed for pamac-aur and pamac-all packages)

How to clone this repository:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/pamac/archlinux-appstream-data-pamac.git
```

